const express= require("express")
require("dotenv/config")

const home=require("./routes/usershema.routes");
const app=express();

app.use('/',home);
app.listen(process.env.PORT);

const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/firstDB", () => {
  console.log("Connected to db");
});